package cl.forum.springboot.springbootcrudrestapivalidation.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.forum.springboot.springbootcrudrestapivalidation.model.Cliente;



@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
