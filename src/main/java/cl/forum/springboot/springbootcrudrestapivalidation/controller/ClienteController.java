package cl.forum.springboot.springbootcrudrestapivalidation.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.forum.springboot.springbootcrudrestapivalidation.exception.ResourceNotFoundException;
import cl.forum.springboot.springbootcrudrestapivalidation.model.Cliente;
import cl.forum.springboot.springbootcrudrestapivalidation.repository.ClienteRepository;

@RestController
@RequestMapping("/api/v1")
public class ClienteController {
	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping("/clientes")
	public List<Cliente> getClientes() {
		return clienteRepository.findAll();
	}

	@RequestMapping(value = "/clientes/{rut}", produces = "application/json", method = RequestMethod.GET) 
	public ResponseEntity<Cliente> getClientesById(@PathVariable(value = "rut") Long rut)
			throws ResourceNotFoundException {
		Cliente cliente = clienteRepository.findById(rut)
				.orElseThrow(() -> new ResourceNotFoundException("no se encuentra cliente para el rut  :: " + rut));
		return ResponseEntity.ok().body(cliente);
	}

	@PostMapping("/clientes")
	public ResponseEntity<Cliente> crearCliente(@Valid @RequestBody Cliente cliente) {
		 clienteRepository.save(cliente);
		 return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
		
	}

	@RequestMapping(value = "/clientes/{rut}", produces = "application/json", method = RequestMethod.PUT) 
	public ResponseEntity<Cliente> actualizarCliente(@PathVariable(value = "rut") Long rut,
			@Valid @RequestBody Cliente clientedetalle) throws ResourceNotFoundException {
		Cliente cliente = clienteRepository.findById(rut)
				.orElseThrow(() -> new ResourceNotFoundException("no se encuentra cliente para el rut :: " + rut));

		cliente.setNombre(clientedetalle.getNombre());
		cliente.setPaterno(clientedetalle.getPaterno());
		cliente.setMaterno(clientedetalle.getMaterno());
		cliente.setEmail(clientedetalle.getEmail());
		final Cliente updatedCliente = clienteRepository.save(cliente);
		return ResponseEntity.ok(updatedCliente);
	}

	@DeleteMapping("/clientes/{rut}")
	public Map<String, Boolean> EliminarCliente(@PathVariable(value = "rut") Long rut)
			throws ResourceNotFoundException {
		Cliente cliente = clienteRepository.findById(rut)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + rut));

		clienteRepository.delete(cliente);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
