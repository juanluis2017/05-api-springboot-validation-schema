package cl.forum.springboot.springbootcrudrestapivalidation.model;


	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.Table;
	import javax.validation.constraints.Email;
	import javax.validation.constraints.NotBlank;
	import javax.validation.constraints.NotNull;
	import javax.validation.constraints.Size;
	
	@Entity
	@Table(name = "cliente")
	public class Cliente {
	
		private long rut;
	
		@NotNull
		@Size(min = 2, message = "First Name should have atleast 2 characters")
		private String Nombre;
		
		@NotNull
		@Size(min = 2, message = "Last Name should have atleast 2 characters")
		private String Paterno;
		
		@NotNull
		@Size(min = 2, message = "Last Name should have atleast 2 characters")
		private String Materno;
		
		@Email
		@NotBlank
		private String email;
		

	
		public Cliente() {
	
		}
	
		public Cliente(Long rut, String nombre, String paterno,String materno) {
			this.rut = rut;
			this.Nombre = nombre;
			this.Paterno = paterno;
			this.Materno=materno;
		}
	
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getRut() {
			return rut;
		}
	
		public void setRut(Long rut) {
			this.rut = rut;
		}
	
		@Column(name = "nombre", nullable = false)
		public String getNombre() {
			return Nombre;
		}
	
		public void setNombre(String nombre) {
			this.Nombre = nombre;
		}
	
		@Column(name = "paterno", nullable = false)
		public String getPaterno() {
			return Paterno;
		}
	
		public void setPaterno(String paterno) {
			this.Paterno = paterno;
		}
	
		@Column(name = "email", nullable = false)
		public String getEmail() {
			return email;
		}
	
		public void setEmail(String email) {
			this.email = email;
		}
	
		@Column(name = "Materno", nullable = false)
		public String getMaterno() {
			return Materno;
		}
	
		public void setMaterno(String materno) {
			this.Materno = materno;
		}
	
	}
